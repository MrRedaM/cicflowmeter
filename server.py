import subprocess
import os
from fastapi import FastAPI, UploadFile
import shutil
import requests
import uvicorn

app = FastAPI()


@app.post("/analyse-pcap/")
async def upload_pcap(file: UploadFile):
    # Check if the uploaded file is a PCAP file
    if file.filename.endswith('.pcap'):
        # Save the uploaded PCAP file to a temporary directory
        with open("temp.pcap", "wb") as temp_pcap:
            shutil.copyfileobj(file.file, temp_pcap)

        # Run the cicflowmeter command to analyze the PCAP file
        base_filename = os.path.splitext(os.path.basename(file.filename))[0]
        subprocess.run(["cicflowmeter", "-f", "temp.pcap", "-c", f"/nfs/csv/{base_filename}.csv"])

        # Send the output.csv file to the target server
        with open(f"/nfs/csv/{base_filename}.csv", "rb") as csv_file:
            response = requests.post(
                "http://192.168.56.5:30300/analyse-csv/",
                files={"file": csv_file}
            )

        # Clean up temporary files
        os.remove("temp.pcap")

        if response.status_code == 200:
            return {"message": "PCAP file uploaded, analyzed, and CSV sent successfully."}
        else:
            return {"message": "Failed to send CSV file to the target server."}
    else:
        return {"message": "Invalid file format. Please upload a PCAP file."}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
