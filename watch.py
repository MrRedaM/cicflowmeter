import os
import subprocess
import time
import csv
import requests
import traceback
import sys
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from datetime import datetime


pcap_source_folder = "/nfs/pcap/"
csv_source_folder = "/nfs/csv/"

ip_address = '192.168.56.5'

port = '30500'

cicflowmeter_command = "cicflowmeter -f {} -c {}"


def process_pcap_file(file_path):
    try:
        # Check if the file has a .pcap extension
        if file_path.endswith(".pcap") and os.path.getsize(file_path) != 0:
            # Generate the destination file name with timestamp
            # timestamp = datetime.now().strftime("%Y-%m-%d_%H:%M")
            base_filename = os.path.splitext(os.path.basename(file_path))[0]
            destination_file = f"/nfs/csv/{base_filename}.csv"

            # Run cicflowmeter with the new file
            subprocess.run(cicflowmeter_command.format(file_path, destination_file), shell=True)

            # Delete the processed file
            #os.remove(file_path)
    except Exception as e:
        print("Error: {}".format(str(e)))


def process_csv_file(file_path):
    try:
        if file_path.endswith(".csv") and os.path.getsize(file_path) != 0:
            # Read the CSV file
            csv_data = read_csv(file_path)

            # Send HTTP POST requests and write responses to a new CSV file
            result_file_path = '/nfs/classification/classification.csv'
            #for row in csv_data:
            response = send_post_request(ip_address, port, csv_data)
            result_value = response.text if response.status_code == 200 else '0'
            write_to_csv(result_file_path, [os.path.basename(file_path), result_value])
    except Exception as e:
        print("Error: {}".format(str(e)))
        # Get the traceback information
        exc_type, exc_value, exc_traceback = sys.exc_info()
        # Print the traceback
        traceback.print_tb(exc_traceback)
        # Get the line number and position
        line_number = exc_traceback.tb_lineno
        line_position = exc_traceback.tb_frame.f_locals.get('e').__traceback__.tb_next.tb_lineno
        print(f"Exception occurred at line {line_number}, position {line_position}: {exc_type.__name__}: {exc_value}")


# Function to read a CSV file
def read_csv(file_path):
    data = []
    with open(file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(row)
    return data


# Function to send an HTTP POST request to an IP address and port
def send_post_request(ip_address, port, data):
    url = f"http://{ip_address}:{port}"  # Construct the URL with the IP address and port
    response = requests.post(url, data=str(data))
    return response


# Function to write a new row to a CSV file
def write_to_csv(file_path, row):
    file_exists = os.path.exists(file_path)

    with open(file_path, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)

        if file_exists:
            # Check if the first column exists in the CSV file
            with open(file_path, 'r', newline='') as read_file:
                reader = csv.reader(read_file)
                for existing_row in reader:
                    if existing_row and existing_row[0] == row[0]:
                        # Update the value in the second column
                        existing_row[1] = row[1]
                        return
        # If the CSV file doesn't exist, create a new one and write the row
        writer.writerow(row)


class PCAPFileHandler(FileSystemEventHandler):
    def on_created(self, event):
        print("PCAP file created: " + event.src_path)
        if event.is_directory:
            return
        else:
            source_file = os.path.join(pcap_source_folder, event.src_path)
            process_pcap_file(source_file)

    def on_modified(self, event):
        print("PCAP file modified: " + event.src_path)
        if event.is_directory:
            return
        else:
            source_file = os.path.join(pcap_source_folder, event.src_path)
            process_pcap_file(source_file)


class CSVFileHandler(FileSystemEventHandler):
    def on_created(self, event):
        print("CSV file created: " + event.src_path)
        if event.is_directory:
            return
        else:
            source_file = os.path.join(csv_source_folder, event.src_path)
            process_csv_file(source_file)

    def on_modified(self, event):
        print("CSV file modified: " + event.src_path)
        if event.is_directory:
            return
        else:
            source_file = os.path.join(csv_source_folder, event.src_path)
            process_csv_file(source_file)


if __name__ == "__main__":
    print("Start watching " + pcap_source_folder)
    pcap_event_handler = PCAPFileHandler()
    pcap_observer = Observer()
    pcap_observer.schedule(pcap_event_handler, pcap_source_folder, recursive=False)
    pcap_observer.start()

    print("Start watching " + csv_source_folder)
    csv_event_handler = CSVFileHandler()
    csv_observer = Observer()
    csv_observer.schedule(csv_event_handler, csv_source_folder, recursive=False)
    csv_observer.start()

    #try:
    while True:
        time.sleep(1)
    # except KeyboardInterrupt:
    #     observer.stop()
    # observer.join()
